//
//  TargetBApp.swift
//  TargetB
//
//  Created by dongzb01 on 2023/1/4.
//

import SwiftUI

@main
struct TargetBApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
