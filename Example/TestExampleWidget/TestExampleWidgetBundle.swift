//
//  TestExampleWidgetBundle.swift
//  TestExampleWidget
//
//  Created by dongzb01 on 2023/2/13.
//

import WidgetKit
import SwiftUI

@main
struct TestExampleWidgetBundle: WidgetBundle {
    var body: some Widget {
        TestExampleWidget()
        TestExampleWidgetLiveActivity()
    }
}
